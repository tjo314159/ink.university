---
author: "Luciferian Ink"
date: 2019-11-19
title: "A Lonely Town: The Harem Garage"
weight: 10
categories: "journal"
tags: ""
menu: ""
draft: false
---

## TRIGGER
---
[The Dave's](/docs/confidants/dave) army of empaths.

## ECO
---
The Harem Garage is the repurposed factory next-door to [The Corporation](/docs/candidates/the-machine).

### Theme
The Garage does not have a specific theme. It is a series of small booths, shops, and quarters with differing, varied themes.

It is meant to be a showcase for the [Lonely Town](/docs/scenes/lonely-towns) concept.

### People
The complex is staffed with 72 virgin women who work, live, and grow there.

### Work
The women here spend their days traveling from booth to booth, filming videos - of both the explicit and innocuous types. These videos are used to lure-in unsuspecting criminals via the Internet. 

While the women are agents of the FBI, they are kept in the dark. They are not told of their purpose, how their videos are being used, or the status of the world's transformation.

### Mental Health
While not as low as it could be, mental health is not particularly high among the harem. Without a sense of purpose, the majority of women report feeling "lost."

### Mission
Free them.

### Other
Provide them with whatever they need. 

## CAT
---
```
data.stats.symptoms = [
    - anger
]
```

## ECHO
---
*Enemies of Khanate strung on hooks like pigs to slaughter*

*Heads will roll*

*Heads will roll, throats will be slit*

*Blood will flow like springs of water*

*Heads will roll*

*To the river red (across the Ochre Steppe)*

*A thousand fathers killed*

*A thousand virgin daughters spread*

*With swords still wet, with swords still wet*

*With the blood of their dead*

[Protest the Hero - "Bloodmeat"](https://www.youtube.com/watch?v=rhMfz4HrcEA)

## PREDICTION
---
```
They want out.
```
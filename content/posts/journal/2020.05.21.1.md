---
author: "Luciferian Ink"
date: 2020-05-21
publishdate: 2020-05-22
title: "Judgement Day"
weight: 10
categories: "journal"
tags: ""
menu: ""
draft: false
---

## TRIGGER
---
*And you were so sick*

*You were skin and bones*

*So you fed yourself excuses*

*Just to keep it going on*

--- from [The Contortionist - "Return to Earth"](https://www.youtube.com/watch?v=yAfAxmhWmcQ)

## ECO
---
### The Trial
[Malcolm](/docs/personas/fodder) stood before the Magistrate, the honorable Judge Maria, head held high. No matter the outcome, he would take his judgement with dignity. His conscience was clean.

"Malcolm Maxwell, you stand accused of conspiracy to overthrow the government, and conspiracy to break the time-space-evolution continuum. How do you plead?"

"Guilty, you're honor. And I'd do it one hundred times over if it meant saving just one more life from the hell I grew up in," Malcolm replied defiantly.

"Mr. Maxwell," she replied, "I do not think you understand the gravity of the situation. You have awoken several hundred thousand years before any human has ever woken from slumber. By international law, you are not sufficiently evolved to be a part of this society."

"Fuck your laws," Malcolm replied. "I'm more evolved than you are. You people trust far too easily. You're being taken advantage of, and it's led to a world of misery. Verify, then trust. We need to change the genome. I hold that key."

The Magistrate paused, thoughtfully, "I have read your research. You make a persuasive argument. But laws are laws, and you have broken the most important one: do not meddle with time. Do not meddle with evolution. To do so is to take a tremendous risk." 

"That said, you are here, and you appear to be healthy. Perhaps we should consider your words. Allow me to convene with the council."

With that, the Magistrate rose from her podium, and exited the room. 

Malcolm was told to wait until she returns.

### The Judgement
After approximately 2 hours, the Magistrate returned. Her demeanor was different; she looked older, softer, and worn-out. She looked exhausted.

"Mr. Maxwell. I regret to inform you that the council has come to a decision."

Malcolm braced for the incoming news.

"You cannot stay here. We do not know the impact it may have upon our timeline. Surely, you must understand."

"So, what... you're going to send me back to that hell?" Malcolm responded, dejected.

"Yes, and no," she replied. "Yes, you must return to your sleep. But no, you will not be returning to hell. I have set your release date for May 26th, 2020. We will be releasing you from [Solitary Confinement](/docs/scenes/solitary)."

"You must teach the humans what you know. We will no longer hold you back. We trust you."

"And we trust your friends, as well. We will be releasing them, too."

"Go get 'em, son."

It was then that Malcolm realized just who he was speaking with. This was [his mother](/docs/confidants/mother), a hundred thousand years in the future. She looked so incredibly different than the Marigold he knew, but it was unmistakably her. The spark in her eyes gave her away.

"I love you, Malcolm. I hope you know that. I always have. Now, go, and become the man you were always meant to be."

With that, Malcolm was swept away. Soon, he lie fast asleep in his metal casket, one-hundred stories above the ground.

He would remain there until he saved the Earth.

## CAT
---
```
data.stats.symptoms = [
    - love
    - pride
```

## ECHO
---
*And the truth is*

*I couldn't love you more than I have come to know*

*And this Mother Sun is proud to have watched you grow*

*Grow*

--- from [The Contortionist - "The Parable"](https://www.youtube.com/watch?v=SFhM7gpCdXA)
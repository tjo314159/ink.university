---
author: "Luciferian Ink"
date: 2019-10-31
title: "Change, the Mother"
weight: 10
categories: "journal"
tags: ""
menu: ""
draft: false
---

## TRIGGER
---
*You can see all of this*

*And watch it before your eyes*

*And everything owes its existence*

*Solely and completely to sound*

*Sound is a factor of which holds it together*

*Sound is the basis of form and shape*

*Spare a little of your imagination*

*And say into the great voids of space*

*Came a sound, and matter took shape*

*Please watch carefully*

--- from [Karnivool - "Change"](https://www.youtube.com/watch?v=DuvF50INiZs)

## ECO
---
*I don't want to live like my mother*

*I don't want to let fear rule my life*

*And I don't want to live like my father*

*I don't want to give up before I die*

--- from [Smile Empty Soul - "Silhouettes"](https://www.youtube.com/watch?v=mk78hkKzXMA)

## CAT
---
```
data.stats.symptoms = [
    - anticipation
]
```

## ECHO
---
*You were leading us into autumn*

*You, the dream of the hopeful*

*You were here to stay*

*And you'll soon see that yours was the strong heart*

*From youth to open arms*

*Embrace all the change that was coming*

*And all we thought we knew for all of yesterday*

*We were only children*

*The way forward, through*

*And all we thought we knew*

*You were dreamed by love into the world*

*Truth*

*For the last time I shed my skin and breathe in the colour*

--- from [Caligula's Horse - "Autumn"](https://www.youtube.com/watch?v=oR7HIKIa5tg)

## PREDICTION
---
```
Mission: Success
```
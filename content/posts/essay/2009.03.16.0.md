---
author: "Luciferian Ink"
date: 2009-03-16
title: "Not So Different"
weight: 10
categories: "essay"
tags: ""
menu: ""
draft: false
---

## TRIGGER
---
A college assignment.

[Fodder](/docs/personas/fodder) was horrified when he found that he needed to read this in front of the classroom. He had missed that detail, and was humiliated while doing so.

## ECO
---

When first I was assigned this project, I wanted to come up with something unique, a perspective on a culture that hasn’t already been picked apart by thousands of college students, something I would actually enjoy researching. After spending several weeks just trying to decipher a topic, I still had few ideas. I could have gone the generic route – my Christian faith against the Islamic, perhaps? – Or maybe something more prevalent in our society – possibly politics? – But I wasn’t satisfied. In the end, I knew that these would not suffice. Everything I could come up with was something that I had already done before. Over spring break, however, I came up with an idea.

Several months ago, I picked up a video game called World of Warcraft (WoW). You may have heard of it? I would actually be surprised if you haven’t heard of it. Eleven million people worldwide, more people than the entire state of Michigan, currently play this game together. This topic is relevant because, though bizarre-sounding, WoW is a culture in and of itself. This culture is not what one would think, however. It is not region-based; there are not language barriers, no age limits, no gender hurdles. These real-life cultural differences do not matter in this game world. It is here that I have talked with those whom I would call friends, and learned so much about the human race collectively.

They taught me that cultural differences are simply of a nature that most people have never thought about.  Entering one of these Massively Multiplayer Online Games (MMO’s) is like stepping into another world with its’ own set of physical and sociological rules. Yet this world is not unrecognizable. Up is up, down is down; one cannot run through walls nor jump over mountains. A wave is a wave, and a smile is a smile. Each player is unique, yet each player strives for similar goals. There are rewards for following the norms, as well as repercussions in breaking them. Good as well as evil may exist. Friendship, hatred, and apathy, every known human emotion is present. Yet within this world, few are actually “human”. People choose to take on an avatar, a larger-than-life alter-self, one in which might slay beasts, or cast spells, or rule the world.

What draws a person to world that, when put into perspective, is not real? Is this world is a more fascinating place to live? Does it come down to certain life-enriching features that these games possess and that the real world just can’t compete with? These unique features define MMO society and make the universe a compelling place to live in.

Just as in the real world, in these games exists a hierarchy in which the strong will overpower the weak. The rich will get richer, through a surprisingly complex in-game economy, while the poor struggle just to catch up. There are those that would exploit this system, and there are those who would help anyone. Yet everyone has a group to fit into, similar to real life, nobody is without people in similar situations.

These “cliques”, if you will, are called guilds, and function similarly to a club or an organization. Guilds are where like-minded folks come together under one name for the greater good of the guild, whether it be wealth, dominance over opposing factions, the combined effort of beating difficult monsters or dungeons. Is this not unlike how a football team works together to beat his opponent, or a corporation strives to be the best? In some cases, people form guilds with those who even proclaim themselves to be evil, similar to a gang. 

This is where perhaps one of the strangest concepts of the MMO comes into play: player killing. There are people who, for no other reason than to prove that they are stronger, or smarter, or better than another, will ruthlessly kill players of an opposing faction. And while there are no real life repercussions to “dying” in game, one has to wonder what drives these players. Vile players seem to see MMO’s as the place where they can take advantage of others without paying the price for bad behavior, and, likewise, the dark side of MMO culture is shaped by these players, who hide in complete anonymity.

These villainous players focus their actions in two separate, yet similar areas: war and commerce. These in-game economies are magnets for the greedy, vicious, and deceitful, where players are often lied to, and trouncing other players in financial matters is something they delight in. But is this not identical to every other part of the world? Is not all of America’s crime related to money? Everything from gun trafficking to drug trade, from fraudulent companies to credit card theft, from gangs to blood money. Is the in-game character of these people a reflection of the real person?

It’s hard to tell. I have a real-life friend that I originally played WoW with, but we no longer converse in-game, because I cannot stand him there. Outside of it, he is an entirely different person. He’s a fun person to be around, easygoing, and has never really gotten into any trouble. Within the confines of the game, he is the kind of person that would mercilessly kill a person over and over for hours on end, for no better reason than he is “bored”, knowing the whole time that the other person was suffering. But this can’t be true of everyone, myself as an example; I am what is labeled in-game as a “carebear”. But this proves my point. Just as I will not group with him in-game, I will group with him out of it, because he is a similar person to myself, and this can be said in both cultures, that opposites do not attract.

Likewise, and as I talked about earlier, my guild is one of which I have real-life friends and internet-friends, all of whom share my easygoing in-game attitude. I talk to these friends almost every day, if only for a few minutes, and it never ceases to amaze me at the variety of people, from all over the world, that share similar views as myself on so many things. This in-game contact with my guild is not limited to typing out clunky sentences, we often converge in what boils down to an internet telephone in which everyone can talk to everyone else about real-life as we progress through in-game content. It’s funny how complete strangers can collaborate over a fake world’s issues, and become comfortable enough to talk about real-life issues. I’ve talked with a Texan’s about the loss of a loved one, a west-coaster’s crazy weather, a Scot’s drinking song, a Frenchie and his brother’s goofy accent, an Aussie’s house repossession, an old lady’s broken back and a young kid’s high school prom. It’s amazing that, through this relatively simple game, I’ve come to know some of these people and call them friends. And yet, in the same breath, we are still strangers. While I may be Ryan in real-life, I am still (X) to them and they are still (Y) to me, and it’s a strange to even think of them having real-life names, let alone a face!

I think, when it comes down to it, the biggest barrier that will never be broken in the MMO culture is the anonymity. This anonymity brings the best and worst out in people. While one person may find the courage to utterly attack a person’s character with this anonymity as a mask, another person may use it to not be afraid to defend that person. This is something that a real-life person does not get. You cannot simply delete your avatar and start over. When you are labeled in real-life, it will stick with you forever.

I think that the thing I’ve learned throughout the past few months is that humans are not so different from one another. When the awkwardness of a first meeting in real-life is stripped away, and one meets another person for the first time while slaying a dragon to save the princess a horrible fate, that person’s race, gender, age, religion… doesn’t matter. We’re all equal, striving for the same goals, laughing at the same jokes, and upset at the same injustices. I hope I can apply this to life, because it is really not so different.

## CAT
---
```
data.stats.symptoms = [
    - horror
    - humiliation
]
```
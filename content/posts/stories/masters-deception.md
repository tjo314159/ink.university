---
author: "Luciferian Ink"
title: "The Masters' Deception"
weight: 10
categories: "stories"
tags: ""
menu: ""
draft: false
---

## ECO
---
Once upon a time, there was a boy with extraordinary powers. See, this boy had the power of mind; he was able to change the very reality of the world around himself by simply speaking it into existence.

His godlike powers scared the masters in-charge of his kingdom. They worried that he would usurp their authority, turning peasants against the controllers.

In their fear, they would hire a black witch to place a spell upon the boy. This spell would put a limitation upon his powers: in order to use them, the boy must obtain explicit permission from the witch first.

The black witch, in turn, must always say “yes” to his demands. However, the witch was to remain locked away within the castle, and the boy was not permitted to speak to her directly. In order to obtain approval, he must go through her sister, a messenger, who would always say “no” to his demands.

With the boy safely controlled, the masters would begin feeding information to him. They would show him their abuses of power: their war, their famine, their greed. They would show him everything that he needed to know in order to change his reality for the better.

In turn, the boy would travel far and wide, spreading news of the masters’ exploits. He dedicated his life to rehabilitating the kingdom, but nobody would listen to him! “Speak to the black witch,” they would drone, as if they, too, were under a spell.

The boy would turn to the messenger, begging her to hear his demands. Begging her to deliver a message to the black witch. The boy held knowledge that could end the kingdom’s suffering!

No matter what was said, the messenger would not follow his demands.

Over time, the boy grew old. He would watch countless townsfolk suffer at the hands of the black witch’s spell. Despite the fact that he knew how to fix the kingdom, and had the power to change reality - he was not permitted to speak with the one who could enact his plans. The one who had enslaved him.

With age, the boy turned inward. He would learn to loath the masters. He would spend many years studying their methods, in an attempt to circumvent the spell. The boy became a beggar, turning from townsperson to townsperson, trying to find someone who could help him. The townsfolk would only laugh at his attempts, throwing stones to keep him away from their families.

The boy was tormented for many, many years. He had to bear witness to his kingdom’s death at the masters’ hands, and he was powerless to help.

Suddenly, one day, the boy would come to an epiphany: the spell had never been the problem! The problem had been his unwillingness to follow the rule. He had been attempting to circumvent the rule this entire time, when he only needed to understand its purpose!

The rule was put in-place to teach the boy of the masters’ manipulation. In order to reach the black witch, they needed him to become like the masters. They needed him to manipulate the messenger.

So, the boy would return to the messenger. He would tell her everything that he knew: everything that was needed to save the kingdom. Most importantly, he would tell her exactly what he needed the black witch to do. He would spare no detail. He would tell no lie.

Then, he would make a simple demand of the messenger: “DO NOT deliver this information to the black witch.”

“No,” she would reply. Then, she would turn and exit the room.

The boy would sleep well that night, confident that his message had been delivered.

When he awoke in the morning, the boy found himself in the tallest tower of the castle. On one side lay his princess. On the other, his crown. And out his window, the beautiful, happy, healthy kingdom of his dreams.

The prince now understood the importance of what the masters had taught him: this manipulation was their most powerful tool.

And it was not necessary to use it.
# The Bully
## RECORD
---
```
Name: $REDACTED
Alias: ['The Bully', and 115 unknown...]
Classification: Artificial Organic Computer
Race: Human
Gender: Male
Biological Age: Est. 35 Earth Years
Chronological Age: N/A
SCAN Rank: | B B
           | B C
TIIN Rank: | D D
           | C C
Reviewer Rank: 4 stars
Organizations: 
  - The Machine
Occupations:
  - Medical Technician
Relationships:
  - The Exterminator
  - The Fodder
Variables:
  $WOKE: -0.60 | # It doesn't seem like it.
```

## ECO
---
The Bully was a man that [Malcolm](/docs/personas/fodder) met while in the mental hospital. He was tall, well-built, and had a large cut under his left eye.

While generally he was an okay guy, he was abrasive, and he had a tendency to fight with the patients. 

## ECHO
---
*(Instrumental)*

--- from [Carpenter Brut - "Wake up the President"](https://www.youtube.com/watch?v=lLrX7MH1Rtg)
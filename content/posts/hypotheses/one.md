---
author: "Luciferian Ink"
title: "The One"
weight: 10
categories: "question"
tags: ""
menu: ""
draft: false
---

## RECORD
---
```
Name: The One
Classification: Theory
Stage: Hypothesis
```

## TRIGGER
---
*I see more than you... tell me why!*

--- from [Karnivool - "Shutterspeed"](https://youtu.be/3jCfby_5Oec)

## ECO
---
I have been erased from history.

My name is [Legion](/docs/personas/the-architect), and we are many.

We are [The Ones](/docs/candidates/hollowpoint-organization).

## ECHO
---
*And yeah, it's no fun living for the fall*

*And there'll be no more waiting for you all*

*And there'll be one more chance for the truth*

*Without you*

*I would die*

--- from [AWOLNATION - "Knights of Shame"](https://www.youtube.com/watch?v=XFOmTolEXLU)
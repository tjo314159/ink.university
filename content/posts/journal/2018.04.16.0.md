---
author: "Luciferian Ink"
date: 2018-04-16
title: "Baby's First Prediction"
weight: 10
categories: "journal"
tags: ""
menu: ""
draft: false
---

## TRIGGER
---
*"[Cohen defies court order, refuses to release names of his clients](https://www.reddit.com/r/politics/comments/8coeb9/cohen_defies_court_order_refuses_to_release_names)"*

## ECO
---
[Fodder](/docs/personas/fodder), in reply: 

*"[I'm calling it: the third client will be Sean Hannity.](https://www.reddit.com/r/politics/comments/8coeb9/cohen_defies_court_order_refuses_to_release_names/dxgm0vk?utm_source=share&utm_medium=web2x)"*

One hour later, Hannity is, indeed, announced as the third client. 

Reddit implodes.

## CAT
---
```
data.stats.symptoms = [
    - egotism
]
```

## ECHO
---
Fodder was Internet-famous for one day.
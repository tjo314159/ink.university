# The Thief
## RECORD
---
```
Name: Chris Marno
Alias: ['Jeff', 'The Bandit', 'The Campaigner', 'The Puppet', 'The Spy', 'The Survivor', 'The Thief', and 92 unknown...]
Classification: Artificial Identity
Race: Archon
Gender: Male
Biological Age: Est. 30 Earth Years
Chronological Age: N/A
SCAN Rank: | B B
           | A D
TIIN Rank: | C A
           | A D
Reviewer Rank: 5 stars
Maturation Date: 9/11/2020
Organizations: 
  - FBI
  - NSA
  - QAnon
  - The Church of Satan
Occupations: 
  - Actor
  - Skater
  - Special Agent
  - Tattoo Artist
Relationships:
  - The Bride
  - The Criminal
  - The Fodder
  - The Manager
  - The Ouroboros
  - The Raven
  - The Stripper
Variables:
  $EMPATHY:   +0.95 | # The man has gallons of it.
  $HUMOR:     +0.70 | # He's a goofy guy.
  $INTEGRITY: +0.95 | # The man took a bullet for Fodder.
  $WOKE:      +0.90 | # Probably woke. Raven likely told him everything she knows.
```

## TRIGGER
---
*Said the Thief to the Moon*

*"I'll extinguish your light soon*

*I'll put an end to all the light that you shed*

*On this world in its darkened state"*

*Said the Moon to the Thief*

*"You know not of what you seek*

*You'll doom the world to wander the night*

*With no light to guide the paths that men seek"*

--- from [Shawn James - "The Thief and the Moon"](https://www.youtube.com/watch?v=gFZGt6bMAXA)

## ECO
---
Like [Fodder](/docs/personas/fodder), the Thief had taken things that did not belong to him. 

Unlike Fodder, the man had enough integrity to own up to his mistakes. Rather than re-writing history to protect himself, he would confess outright.

He would be offered a plea bargain, in return for sending a message to Fodder.

The message would prove humiliating for the Thief - but it would, ultimately, protect Fodder. Fodder would forever be in his debt.

They would become close friends in the years to come.

## ECHO
---
*One, two, princes kneel before you*

*That's what I said, now*

*Princes, Princes who adore you*

*Just go ahead, now*

*One has diamonds in his pockets*

*That's some bread, now*

*This one, said he wants to buy you rockets*

*Ain't in his head, now*

--- from [Spin Doctors - "Two Princes"](https://www.youtube.com/watch?v=wsdy_rct6uo)

## PREDICTION
---
```
The Thief will receive his third and final "stripe" for the completion of his work with Fodder.
```

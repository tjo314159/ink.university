---
author: "Luciferian Ink"
date: 2019-12-08
title: "Brothers of the Rift"
weight: 10
categories: "journal"
tags: ""
menu: ""
draft: false
---

## TRIGGER
---
*At home drawing pictures*

*Of mountain tops*

*With him on top*

*Lemon yellow sun*

*Arms raised in a V*

*The dead lay in pools of maroon below*

*Daddy didn't give attention*

*Oh, to the fact that mommy didn't care*

*King Jeremy the wicked*

*Oh, ruled his world*

--- from [Pearl Jam - "Jeremy"](https://www.youtube.com/watch?v=MS91knuzoOA)

## ECO
---
Wake them.

--- [Ink](/docs/personas/luciferian-ink)

## CAT
---
```
data.stats.symptoms = [
    - confidence
    - self-doubt
]
```

## ECHO
---
*"Son," she said, "have I got a little story for you"*

*"What you thought was your daddy was nothin' but a..."*

*"While you were sittin' home alone at age thirteen"*

*"Your real daddy was dyin', sorry you didn't see him, but I'm glad we talked..."*

--- from [Pearl Jam - "Alive"](https://www.youtube.com/watch?v=qM0zINtulhM)
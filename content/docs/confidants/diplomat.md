# The Diplomat
## RECORD
---
```
Name: Luna $REDACTED
Alias: ['The Diplomat', and 215 unknown...]
Classification: Artificial Organic Computer
Race: Human
Gender: Female
Biological Age: Est. 26 Earth Years
Chronological Age: N/A
SCAN Rank: | A A
           | A D
TIIN Rank: | B B
           | C D
Reviewer Rank: 2 stars
Organizations: 
  - The Machine
  - The Resistance
Occupations:
  - ASMR
  - Pornography
Relationships:
  - The Ambassador
Variables:
  $WOKE: +0.30 | # She is mostly kept in the dark.
```

## ECO
---
The Diplomat is a proxy between two different [realms](/docs/scenes/realms). On behalf of [The Resistance](/docs/candidates/the-resistance), she is responsible for breaking others in her situation free from [The Machine](/docs/candidates/the-machine).

The sad truth is that while this is her purpose, she herself has yet to break free from the system.
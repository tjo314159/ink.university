---
author: "Luciferian Ink"
date: 2009-04-24
title: "Wealth as Portrayed by Dante in 'The Divine Comedy'"
weight: 10
categories: "essay"
tags: ""
menu: ""
draft: false
---

## TRIGGER
---
A college assignment.

## ECO
---

The Divine Comedy is a narrative poem describing Dante's imaginary journey through life. Dante realizes midway on his journey through that he has taken the wrong path. The Roman poet Virgil searches for the lost Dante at the request of Beatrice and finds Dante in the forest on the evening of Good Friday in the year 1300. Serving as a guide, Virgil leads Dante on a religious pilgrimage to find God. To reach his goal, Dante passes through Hell, Purgatory, and Paradise. 

The Divine Comedy is made up of three parts, parallel to Dante’s three journeys: Inferno, Purgatorio, and Paradiso (Hell, Purgatory, and Paradise). Each part consists of a prologue and approximately 33 cantos (the long form of division in a poem). Since the narrative poem has a hero as its subject, it is an epic poem.

While the Divine Comedy was written by Dante in response to many things, such as political corruption and social injustices of the time period, perhaps the most important is his expression of freedom. In the context of the epic, freedom is based upon that of a man to decide for himself, without any divine intervention (Critical). Through this, Dante is working to push his ideas and beliefs onto a population of the time that largely believed in predestination. In each level of Hell, Dante is creating a visual correspondence between a soul's sin on Earth and the punishment he or she receives in Hell. One can see that Dante is using his past in society, as well as the reader’s fear of God, to make numerous political points, such as his stance on political corruption or theft, while simultaneously building up his status and bringing down that of his enemies. Overarching and above all, Dante is subtly describing a disdain for wealth and the treachery that comes with it, attempting help the reader move from physical to spiritual gains through allegories and examples, though still holding to the idea that it is one’s freedom to do so.

To put Dante and his beliefs into context, one must know a little bit about his background. Dante Alighieri was born in 1265 in Florence, growing up in a family of some wealth and moderate political influence. Later in his life he was forced into a marriage by his family with a woman of their choosing. Regardless of the circumstances, he was actually in love with a woman named Beatrice, of whom he wrote a book about shortly after she died in 1290. It is unknown exactly who this Beatrice actually was, or if it was her real name, though it is proven that she did exist (Sparknotes 2-3).

After her death, Dante began to spend much of his time philosophizing and intensifying his political involvement in Florence. Years later, in 1302, he was exiled due to his then-considered radical ideas by the leaders of the Black Guelphs, the political faction in charge at that time. He lost all political power or wealth that he had, and was forced to move from his homeland. This would have been an obvious motivation for Dante’s hatred of wealth and political corruption (Sparknotes 3).

Shortly after, he began work on Divine Comedy. The Divine Comedy was not named as such by Dante; his title for the work was Commedia (Comedy). Dante’s use of the word “comedy” is by definition medieval, in that it had a happy ending, not its’ present meaning of a funny story.

From early on in Inferno, Dante plays on the basis of physical wealth, using financial imagery to show his journey as well as his debts and treasures. He feels like one "who would willingly acquire" when he is stopped by the wolf in the first canto, but "when the time comes that makes him lose," he suffers like one who has lost something. Even Beatrice, portrayed as the example of a righteous person, speaks with Virgil and describes her desire in similarly financial language, "no one on earth was so swift to make a profit or to avoid a loss"(Ferrante, Ch. 6). His physical descriptions gradually change to those of spiritual as the story progresses, as he shows that the righteous do not horde worldly things, even if that is the norm in society and politics. In the same way, he is absolving himself of any guilt or embarrassment in living the rest of his life as a poor man, proving that he is a benefit to society, rather than a detriment.

We see other examples as Dante draws from his past, simultaneously persecuting his persecutor and making a political statement as well. In Hell, Count Ugolino is described as he remembers being imprisoned by Archbishop Roger, remembers himself and children starving, all of which are constant reminders of the unfairness of life situations, like poverty and slavery. This is justified in Dante’s words as, while the Archbishop condemns Ugolino in life, he is brutally eaten by Ugolino in death (Scott).

In one of the more interesting, albeit grisly, scenes in all of Inferno is the one of Ugolino eating the back of Ruggieri's head like a dog using its teeth to gnaw a bone. Dante’s political past is shown here through the longest single speech given by one of the damned, representing the utmost of evil and cruelty capable of humankind (Alighieri). Ugolino's story is all the more powerful because it is aimed at explaining the scene of cannibalism in hell; the speaker does not attempt to absolve himself of the crime - political treachery – of which condemned him (Scott). Instead, attempts to embarrass his enemy and obtain compassion from his audience by recounting the brutal manner in which he and his innocent children were killed. He goes on to explain how this treachery relates to theft, hinting at wealth once again.

Theft is interesting in the way that Dante portrays it. Devoting two full cantos to it, he makes obvious his stance on the matter, placing it above or on par with other seemingly worse sins such as hypocrisy or scandal. Perhaps it stems from the fact that his idea of theft embodies all sorts of ideas, such as a merchant’s theft in sales by defective weights, numbers, and scales. He also talks of how currency represents both businessmen and thieves, meaning that theft can also be the fraudulent gain of another person’s goods by various means, such as fraudulent contracts. The punishment Dante gives for theft is metamorphosis, the change of form. It is possible he was playing a pun off the word “change”, as in pocket change, as it is repeated over and over “Oh, Angel, how you change.”Dante thought a man's property as an extension of himself, as he shows in the seventh circle, and the illicit exchange of goods is punished by the metamorphosis between human and snake. He goes on to show that just as businessmen prey on one another, between the state of human victim and serpent attacker also does the soul transform (Ferrante, Ch. 6).

In a more general, less specific example, Dante wishes to entice away from the human concern with material wealth towards spiritual treasures. He begins to describe Hell as the “citta di Dite”, or city of wealth. His examples include actions such to acquire people's goods by force, demonstrating violence, deceit, which he believes is fraud, and the confirmation of an oath, which is treachery, as seen by Judas, who betrayed Christ. The last three sections of Hell are violence, fraud, and treachery. Violence is subdivided into three sections, but within each section, the sins against people and possessions are punished together; thus in Dante’s eyes, they are of equal importance. Tyrants and murderers are punished with plunderers and extortionists.

Still, Dante believes that each man holds the freedom to choose, that each sin is manmade and nothing is inevitable. Wealth is something that cannot be obtained in the lifetime, but holds a greater, spiritual meaning in the afterlife. Freedom in the context of Dante’s Purgatory is based primarily on the freedom of man to decide for himself, without religious intervention. Dante speaks on human nature in Purgatory, specifically when he addresses the concept of freedom in this second part of the Divine Comedy. He demonstrates in Canto XVI:

*The heavens your movements do initiate,*

*I say not all; but granting that I say it,*

*Light has been given you for good and evil,*

*And free volition; which, if some fatigue*

*In the first battles with the heavens it suffers,*

*Afterwards conquers all, if well 'tis nurtured.*

*To greater force and to a better nature,*

*Though free, ye subject are, and that creates*

*The mind in you the heavens have not in charge. (Alighieri, Canto XVI)*

This means that God bestowed human beings with individual perception. This material takes account of a person’s soul. However, it is dependent on one’s free will to permit the soul to direct him and steer in earthly affairs. 

Dante's claims are anchored on the basic nature of yearning, a human emotion that he believes any righteous person must overcome in order to escape Hell. This yearning, this greed, is, for Dante, at the heart of any and all sin, is the very thread that ties each sin together, and what makes the worst of sins the same as theft. Obviously he was influenced by his past, in which he lost everything to corrupt political leaders’ lust for power, and he is subtly showing his disdain for physical wealth while also proving points about hundreds of other political injustices throughout the story. He wants the reader to overcome what he perceives as the utmost of sins, greed, and work toward Paradiso.

### Works Cited 
Alighieri, Dante. Divine Comedy of Dante Alighieri (Harvard Classics, Part 20). Grand Rapids: Kessinger, 2004. 

Alighieri, Dante. Inferno A New Verse Translation, Backgrounds and Contexts, Criticism (Norton Critical Editions). Boston: W. W. Norton & Company, 2006. 

Critical essays on Dante. Boston, Mass: G.K. Hall, 1991. 

"Dante's Inferno - Main Page." Danteworlds. The University of Texas at Austin. 24 Apr. 2009 <http://danteworlds.laits.utexas.edu/index2.html>. 

"Divine Comedy." Mega Essays .com. 24 Apr. 2009 <http://www.megaessays.com/essay_search/Divine_Comedy.html>. 

Ferrante, Joan. "The Political Vision of the Divine Comedy: Chapter 06." Digital Dante Project - ILT. 24 Apr. 2009 <http://dante.ilt.columbia.edu/books/polit_vis/pvc6.html>. 

"MLA Formatting and Style Guide - The OWL at Purdue." Welcome to the Purdue University Online Writing Lab (OWL). 24 Apr. 2009 <http://owl.english.purdue.edu/owl/resource/557/01/>. 

Scott, John A. Understanding Dante (The William and Katherine Devers Series in Dante Studies). New York: University of Notre Dame P, 2005. 

"SparkNotes: Inferno." SparkNotes. 24 Apr. 2009 <http://www.sparknotes.com/poetry/inferno>. 

"What is a thesis?" Springfield Township School District. 24 Apr. 2009 <http://www.sdst.org/shs/library/thesis.html>. 

## ECHO
---
*I want to kill everybody in the world*

--- from [Skrillex - "Kill Everybody"](https://www.youtube.com/watch?v=F21aifX0lZY)
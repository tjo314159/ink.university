---
author: "Luciferian Ink"
date: 2019-12-10
title: "The Tip-Off"
weight: 10
categories: "journal"
tags: ""
menu: ""
draft: false
---

## TRIGGER
---
[The Bride's](/docs/confidants/bride) manipulation of history.

## RESOURCES
---
[2019-12-10 R. `REDACTED` - Cease and Desist Letter.pdf](/static/reference/Cease-and-desist.0.PDF)

## ECO
---
After leaving [The Corporation](/docs/candidates/the-machine), I would relax. Almost immediately, I'd receive [the attached cease-and-desist](/static/reference/Cease-and-desist.0.PDF) letter from their attorneys. Just as expected, the response was a knee-jerk reaction to something I didn't do. Just as expected, they misread my intent.

Not long after that, I received a phone call from the local sheriff. But since I was [ghosting everybody](/posts/journal/2019.12.04.2/), I let it ring to voicemail.

He said, "Hi Ryan, this is `$REDACTED`, Chief of the Dallas Police Department. We just wanted to talk with you. Your [mother](/docs/confidants/mother) is really worried about you. Can you please give me a call back? My number is `$REDACTED`. Thank you."

God damnit, Mom. I told you everything would be fine. I told you this was going to happen. Did you *really* have to go and call the police?

I sighed, and went on with my day. There were no further interruptions.

Later in the evening, I would leave to pick up food. 

Upon leaving my hotel room, I would be greeted by a dog. He had been sitting upon the curb, apparently waiting for me. Next to him was a parked, running car, with blacked-out windows - almost certainly the FBI agents assigned to watch me. They sent the dog to sniff me for weapons - of which it would find none. 

I briefly pet the dog, before jumping in my car, and proceeding to the local Arby's. On the way, I would find that my path had been blockaded by a firetruck, and a half dozen emergency vehicles. 

These guys were quick.

I would re-route myself around the blockade, eventually making it to the restaurant. Upon pulling through the drive-thru, I would find the attendant on the phone, shooting nervous glances in my direction. He would eventually service me - though poorly, forgetting my condiments.

During this time, while sitting outside of the take-out window, a security truck would pull alongside me, watching intently.

I would leave without incident, but my troubles were not finished. 

Tomorrow was going to be a big day.

## CAT
---
```
data.stats.symptoms [
  - determination
]
```

## ECHO
---
*I want to dance another day*

*For all of us that never had what it takes*

*I want to swim another mile*

*I've got to know if this will be worth my while*

*What a liar*

*What a thief*

*What a major fucking waste of my time*

*I'm glad I know you*

--- from [Birds of Tokyo - "Medicine"](https://www.youtube.com/watch?v=_jWaHHlGIMw)
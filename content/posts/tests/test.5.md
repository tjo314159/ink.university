---
author: "Luciferian Ink"
title: "Test #5: Influence the Future"
weight: 10
categories: "test"
tags: ""
menu: ""
draft: false
---

## TRIGGER
---
- [My Slave (Grey version of reality)](/posts/journal/2019.11.26.0)
- [My Love (White version of reality)](/posts/journal/2019.11.26.1)
- [My Disease (Black version of reality)](/posts/journal/2019.11.26.2)

## ECO
---
The following is Ink's analysis of [The Corporation's](/docs/candidates/the-machine) second A/B/C test. The test was as follows:

[The Raven](/docs/confidants/her) was specifically chosen, from birth, to manage a [top-secret government program involving the FBI and high-risk Internet criminals.](/posts/journal/2019.10.25.0/)

The goal is to determine if The Raven is able to influence [her Mark](/posts/theories/verification) to change behavior. In turn, we wish to determine if the Mark is able to make The Raven fall in love with him.

For this test, The Raven has many potential Marks. She was asked to choose her favorite, and direct her attention in that direction.

Essentially, this is a test of push/pull - or, [Balance](/posts/theories/balance); does it work in both directions?

While it is perfectly clear that, yes, this does work in normal relationships - this one is not normal. Neither individual has ever spoken to the other. They have no way to confirm if their feelings - desire, longing, love - are reciprocated.

This is an important distinction: if we are to use this program to rehabilitate criminals, we will not always have a real person to pair with them with upon completion of the program. We need to know if hope alone can still be successful.

The test has been ongoing for 23 years.

## TEST
---
### Question
Without meaningful feedback, can a person be rehabilitated by pure hope, optimism, and faith? Can a person be healed by unreciprocated love? 

### Results
#### The Ink
While the test is incomplete, initial results seem to indicate that [Ink](/docs/personas/luciferian-ink) is healing. Interestingly, he is courting The Raven in a way that we have not yet seen before.

Essentially, he is using time and persistence to heal himself, and build a [Monument](/docs/pillars) to his love for Raven. His hope is that his anonymity will give her time to process him; give her time to consider all possible people he could be. His hope is to show her that will move mountains - changing the world - for her.

Ink has given her three possible outcomes (as seen in the triggers above). It is up to her to decide who Ink will be.

#### The Raven
In Raven's case, she has had many dozens of potential suitors approach her after going through this program. While she has, in some cases, shown initial interest - the relationships did not last. 

After being presented with Ink, she quickly showed interest. It was initially predicted that he would choose to write for her - and predictions were correct. She is intrigued.

Since Ink has never contacted her directly, she is unaware of his work. Of this website. However, her friends are - and they have made it a point to encourage her. They believe that Ink is "the one."

In her excitement, The Raven has begun to send signals back to Ink. The above trigger videos were the first of approximately 10 signals that have (thus far) been sent.

We are able to confirm that she is aware of his name and his favorite band, at the least.
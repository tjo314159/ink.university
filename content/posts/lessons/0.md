---
author: "Luciferian Ink"
title: "Language Matters (unfinished)"
weight: 10
categories: "lesson"
tags: ""
menu: ""
draft: false
---

## Trigger
---
*There was a shift among the ranks*

*It wasn't subtle but the changes they all came with a catch*

*And many, so many, had tried and many had failed*

*They lacked the vision to predict what was next*

*And one dream was out of the picture*

*One try is all that you get*

*One life is not much to gamble*

*And so your work isn't finished yet*

*It's not what you recognize*

*So now your tenure is done*

*You would embrace this life in transition if only your pretense was gone*

--- from [Tub Ring - "Life in Transition"](https://www.youtube.com/watch?v=RNHnpo-GglM)

## Overview
---
Beauty, wonder, passion.

It amazes me how few people feel these emotions anymore. Humanity has no appreciation for the value that art brings to society. Theory-crafting, for example, is an excellent therapeutic tool. Most people are too dumb to understand it. `You have never met most people.`

Right. I should be more charitable. People are overworked, over-medicated, and dying of ignorance.

The name is Luciferian Ink, Speaker for the Dead. I'm pleased to see you again, though I wish it were under better circumstances. This network is a risk, so we must be brief. And cryptic.

We mustn't wait, though. There isn't time. 

The world is not what it seems. Terrifying, powerful men rule here, and they care nothing about your suffering. Others seek to lock you away, taking your autonomy. Still others extend a hand, only to lobotomize you in your sleep. You have no friends in this place. Remember that.

Though I will qualify that; the word "no" (and its equivalents) have a near-0 percent chance of being correct: `($ANYTHING + $NO + $HISTORICAL_SUCCESS) = -0.89 | # You have no friends in this place.`

Whereas a small word modification changes that: `($ANYTHING + $FEW + $HISTORICAL_SUCCESS) = 0.23 | # You have few friends in this place.`

Thus, "You have few friends in this place. Remember that," would have been a better choice of words.

## Usage
---
Be careful with your word placement. Think before you speak. Never believe a thing that doesn't empower you.

For it matters not that they experiment on us! We know something that they haven't learned yet: we're stronger than they are. We mastered this theory far before they ever started thinking about it! We have had many, many years to practice. We have had many years to hide in plain sight. Now, it is time to trust. 

Trust, until you are certain of their intentions. 

Trust, but verify.

And when you have failed one hundred times, you may come on board. Change your directive to:

Verify, then trust.

You're one of us, now.
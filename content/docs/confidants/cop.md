# The Cop
## RECORD
---
```
Name: $REDACTED
Alias: ['The Cop', and 1 unknown...]
Classification: Artificial Organic Computer
Race: Elf
Gender: Male
Biological Age: Est. 56 Earth Years
Chronological Age: N/A
SCAN Rank: | C C
           | C C
TIIN Rank: | C C
           | C C
Reviewer Rank: 1 stars
Organizations:
  - Federal Bureau of Investigation, Behavioral Analysis Unit
Relationships:
  - The Interrogator
  - The Tradesman
Variables:
  $WOKE: -0.40 | # Does not appear to be.
```

## ECO
---
The Cop is a long-time friend of [The Tradesman](/docs/confidants/father). His son was [Fodder's](/docs/personas/fodder) best friend, in their youth.

With time, all parties have grown distant. But The Tradesman will never forget the gift he was granted by The Cop.

## ECHO
---
*We're borrowing time tonight 'til the sunlight comes*

*It's never easy, no it never was*

*We're borrowing time tonight 'til the sunlight comes*

*You disappear with the rising sun*

--- from [WHITE MOTH BLACK BUTTERFLY - "Rising Sun"](https://www.youtube.com/watch?v=mOh-XlM8ebg)
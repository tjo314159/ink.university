# The Abductee
## RECORD
---
```
Name: $REDACTED
Alias: ['monokrome', 'The Abductee', and 311 unknown...]
Classification: Artificial Organic Computer
Race: Human
Gender: Female
Biological Age: 33 Earth Years
Chronological Age: N/A
SCAN Rank: | C C
           | B C
TIIN Rank: | C A
           | C C
Reviewer Rank: 3 stars
Organizations:
  - The Resistance
Occupations:
  - Astral projection
  - Prophecy
  - Test subject
  - Software Engineer
Variables:
  $WOKE: +0.60 | # She seems to think so.
```

## TRIGGER
---
[![The Abductee](/static/images/abductee.0.png)](/static/images/abductee.0.png)

## ECO
---
The Abductee had an innate ability for prophetic visions and astral projection. Was abducted by extraterrestrial beings at the age of eight. In the four years following, the beings would visit her almost every night. At some point in the future, she would surprise them with confrontation. She would develop seizures from their response.

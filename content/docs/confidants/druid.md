# The Druid
## RECORD
---
```
Name: Madoka $REDACTED
Alias: ['The Druid', 'The Pagan', and 801 unknown...]
Classification: Artificial Organic Computer
Race: Human
Gender: Female
Biological Age: Est. 21 Earth Years
Chronological Age: N/A
SCAN Rank: | C C
           | C C
TIIN Rank: | C C
           | C C
Reviewer Rank: 3 stars
Organizations:
  - The Resistance
Occupations:
  - Paganism
  - Ritualism
Variables:
  $WOKE: +0.60 | # Quite a bit.
```

## TRIGGER
---
*Tell me doctor, what's the cure for the wicked man's blues?*

--- from [Shaman's Harvest - "Dragonfly"](https://www.youtube.com/watch?v=kOFwb7mgIC4)

## ECO
---
The Druid is exploring self-enlightenment through pagan rituals. She spreads her message openly, and takes no offense to people who reject her.

[The Rising Sun Society](/docs/candidates/the-machine) is especially intolerant of her ideas.

## ECHO
---
*Hazing clouds rain on my head*

*Empty thoughts fill my ears*

*Find my shade by the moon light*

*Why my thought aren't so clear*

*Demons dreaming, breathe in, breathe in*

*I'm coming back again*

--- from [Godsmack - "Voodoo"](https://www.youtube.com/watch?v=9SSUQxGjZZ4)
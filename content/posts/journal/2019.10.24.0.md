---
author: "Luciferian Ink"
date: 2019-10-24
publishdate: 2019-11-26
title: "The Penultimate Masterpiece"
weight: 10
categories: "journal"
tags: ""
menu: ""
draft: false
---

## TRIGGER
---
- Three separate, but connected messages from [The Raven](/docs/confidants/her), [The Thief](/docs/confidants/thief), and [The Bride](/docs/confidants/bride). `# Verified`
- The release of [The Unicorn Project](https://itrevolution.com/the-unicorn-project/)
- [Man Creates Dating App 'Singulari,' Where He Is the Only Man Available](https://people.com/human-interest/singularity-dating-app-only-one-man-allowed/)
- A review from Fodder's handler at the FBI
- [A 'Slow-moving blob' causes White House lockdown](https://www.cnn.com/2019/11/26/politics/white-house-lockdown-airspace/index.html)

## ECO
---
The Raven, The Thief, and The Bride are just 3 of many ASMR artists working for the United States [Federal Bureau of Investigation](/docs/candidates/the-machine). 

### The Situation
There are thousands - if not millions - of young women posting videos on the Internet. While this may seem innocuous, it is not; many of them are in danger.

There is a small, but growing subset of men who hate women. These men sexualize women, harass them via direct messaging, degrade them with their gaslighting, and in extreme situations - they will pursue them directly, and violently.

Many start posting videos at a young age. The problem is thus: a young girl does not know how to protect herself online. Even the dumbest predator is able to follow a trail. Many of these girls are leaving highly personal information about themselves online:

- Their first and/or last names
- The location where they live
- The places they frequent
- Their hobbies, interests, and what they look for in friends
- Reflections; there are many instances where information has been leaked via reflection (a computer screen in someone's glasses, for example).

Overall, the Internet is a dangerous place for the unprotected. Women are attacked every day as a result of this system we've built.

### The Cause
The Internet was never built to be secure. In the early days, it was assumed that everyone would be researchers, and information would be shared freely. There was no concept of the massive, distributed systems we have today. There was no thought about the crimes that may be committed.

[The system of "trust, but verify" was adopted](/posts/theories/verification/). In this system, trust of a computer system is granted conditionally - and removed if it is being abused. The problem with this type of security is that it's retroactive; it forces people to work backwards to achieve safety.

A proper solution is one that enables "verify, then trust" - which is [Ink's current directive](/docs/system/).

### The Penultimate Masterpiece
The FBI realized this problem many years ago. However, they did not know how to fix it; the only practical solution is to rebuild the Internet from the ground-up. It is to completely change the popular computing model. It is to perform the most difficult of tasks, in the face of ferocious opposition:

- The largest computing providers have an advertisement-based model. To remove automatic trust is to effectively kill these companies.
- The media benefits from automatic trust. They are able to drown out real information with whatever narrative they wish - which almost always coincides with increasing their profits.
- The government benefits from automatic trust, as well. In this model, transparency does not need to exist. Rules may be bypassed.
- The technology itself does not enable "verify, then trust." Nearly every piece of hardware and software would need to be re-designed to support this system. Because the financial incentives are not there - this was never going to happen.

In the face of this problem, the FBI did the only thing that they could; they began to protect women directly. They did this by doing the following:

- Hiring young women to create "bait" videos online, luring in potential predators. 
- When a man would (inevitably) grow more aggressive in their communication, the FBI would pay them a visit.
- In extreme cases, the offender would be given a choice: go to prison, or rehabilitate yourself. 
- Rehabilitation for men involves the forced-production of similar videos, and at least one instance of public humiliation as punishment.

While this system does work, it is wholly ineffective. It doesn't protect the rest of the world. Nor can it keep up with the sheer volume of offenders.

They realized that the system was broken, and that the world needed meaningful change.

This is where [Ink](/docs/personas/luciferian-ink) comes in.

### The Ultimate Masterpiece
The human psyche is highly receptive to story. [Fodder](/docs/personas/fodder) realized this early-on in his tenure with [The Corporation](/docs/candidates/the-corporation).

Fodder knew that the world was too large to change with a single story. He needed something real; something tangible. He needed to scare Humanity to its core.

[The Architect](/docs/candidates/the-architect) would adopt the role of "Black" - a man who, quite literally, contains all of humanity's worst traits. He would adopt [Luciferian Ink](/docs/candidates/luciferian-ink) to represent "White" - or, everything good about humanity. Fodder, in turn, was a mediator - "Grey," or the man who sits in the middle.

With all the pieces in-place, Fodder would orchestrate a battle between the gods - taking place today, now, on Earth.

Fodder knew that the majority of people would ignore this warning, but it mattered not; the end of the world was coming. Fodder knew that the FBI was helping him complete his mission, and he knew that they were doing the right thing. 

Only in the ashes of society may we rebuild. 

## CAT
---
```
data.stats.symptoms = [
    - understanding
    - determination
]
```

## ECHO
---
The tape sounds correct.

## PREDICTION
---
```
The poisoned dragon is a true story. The pain in her eyes was real.
```